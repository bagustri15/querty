import * as Cookies from 'js-cookie'

// holds your root state
export const state = () => ({
  token: '',
  user: { name: '', image: '', email: '' },
})

// contains your mutations
export const mutations = {
  setToken(state, value) {
    state.token = value
  },
  setUser(state, value) {
    state.user = value
  },
}

// contains your actions
export const actions = {
  register(context, payload) {
    // console.log('Works')
  },
  login(context, payload) {
    // example axios
    // return new Promise((resolve, reject) => {
    //   axios.post('/assignOrder', assign)
    //     .then((response) => {
    //       resolve(response)
    //     })
    //     .catch((error) => {
    //       reject(error.response.data.errors)
    //     })
    // })

    const randomNumber = Math.floor(Math.random() * 100)
    // Cookies.set('token', `Testing Token ${randomNumber}`)
    context.commit('setToken', `Testing Token ${randomNumber}`)
    context.commit('setUser', {
      name: 'Sandhi Pratama',
      image: 'avatar-5.png',
      email: 'sandhi.pratama@gmail.com',
    })
  },
  logout(context, payload) {
    context.commit('setToken', '')
    context.commit('setUser', {
      name: '',
      image: '',
      email: '',
    })
    Cookies.remove('vuex')
    this.$router.push('/login')
  },
}

// your root getters
export const getters = {
  //   myGetter(state) {
  //     return state.counter + 1000
  //   },
}
