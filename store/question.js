// holds your root state
export const state = () => ({
  listQuestionAll: [
    {
      id: 1,
      name: 'Sandhi Pratama',
      image: 'avatar-5.png',
      question: 'Bagaimana cara mengetes seseorang kaya atau tidak?',
      answer:
        'Kalau diajak jalan-jalan berkendara dengan kendaraan yang dia miliki, perhatikan caranya isi bensin. Kalau diajak jalan-jalan berkendara dengan ...',
      voteUp: 100,
      voteDown: 0,
      createdAt: 1643543658166,
    },
    {
      id: 2,
      name: 'Sandhi Pratama',
      image: 'avatar-5.png',
      question: 'Bagaimana cara mengetes seseorang kaya atau tidak?',
      answer:
        'Kalau diajak jalan-jalan berkendara dengan kendaraan yang dia miliki, perhatikan caranya isi bensin. Kalau diajak jalan-jalan berkendara dengan ...',
      voteUp: 50,
      voteDown: 0,
      createdAt: 1643543658166,
    },
    {
      id: 3,
      name: 'Sandhi Pratama',
      image: 'avatar-5.png',
      question: 'Bagaimana cara mengetes seseorang kaya atau tidak?',
      answer:
        'Kalau diajak jalan-jalan berkendara dengan kendaraan yang dia miliki, perhatikan caranya isi bensin. Kalau diajak jalan-jalan berkendara dengan ...',
      voteUp: 10,
      voteDown: 30,
      createdAt: 1643543658166,
    },
    {
      id: 4,
      name: 'Sandhi Pratama',
      image: 'avatar-5.png',
      question: 'Bagaimana cara mengetes seseorang kaya atau tidak?',
      answer:
        'Kalau diajak jalan-jalan berkendara dengan kendaraan yang dia miliki, perhatikan caranya isi bensin. Kalau diajak jalan-jalan berkendara dengan ...',
      voteUp: 1,
      voteDown: 50,
      createdAt: 1643543658166,
    },
    {
      id: 5,
      name: 'Sandhi Pratama',
      image: 'avatar-5.png',
      question: 'Bagaimana cara mengetes seseorang kaya atau tidak?',
      answer:
        'Kalau diajak jalan-jalan berkendara dengan kendaraan yang dia miliki, perhatikan caranya isi bensin. Kalau diajak jalan-jalan berkendara dengan ...',
      voteUp: 0,
      voteDown: 100,
      createdAt: 1643543658166,
    },
  ], // UNTUK MENYIMPAN DATA DI HALAMAN HOME
  listUser: [
    {
      id: 1,
      name: 'Tito Karnivan',
      image: 'avatar-1.png',
      answer: '12 jawaban di Model Bisnis',
    },
    {
      id: 2,
      name: 'Depa Panjie',
      image: 'avatar-2.png',
      answer: '12 jawaban di Model Bisnis',
    },
    {
      id: 3,
      name: 'Teddy Kurniawan',
      image: 'avatar-3.png',
      answer: '12 jawaban di Model Bisnis',
    },
    {
      id: 4,
      name: 'Lestian Agung',
      image: 'avatar-4.png',
      answer: '12 jawaban di Model Bisnis',
    },
  ],
  detailQuestion: {
    question: 'Bagaimana cara mengetes seseorang kaya atau tidak?',
    following: 15,
    listAnswer: [
      {
        id: 1,
        name: 'Sandhi Pratama',
        image: 'avatar-5.png',
        answer:
          'Kalau diajak jalan-jalan berkendara dengan kendaraan yang dia miliki, perhatikan caranya isi bensin. Kalau diajak jalan-jalan berkendara dengan ...',
        voteUp: 100,
        voteDown: 0,
        createdAt: 1643543658166,
      },
    ],
  }, // UNTUK MENYIMPAN DATA PADA HALAMAN DETAIL PERTANYAAN
  listSameQuestion: [], // UNTUK MENYIMPAN DATA PERTANYAAN YANG SAMA
  historySearch: [], // UNTUK LIST KEYWORD SEARCH
  listQuestionUser: [], // UNTUK MENYIMPAN DATA DI HALAMAN LIST QUESTION
})

// contains your mutations
export const mutations = {}

// contains your actions
export const actions = {}

// your root getters
export const getters = {}
