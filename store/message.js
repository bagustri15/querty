// holds your root state
export const state = () => ({
  data: {
    type: '', // success || error
    msg: '',
  },
})

// contains your mutations
export const mutations = {
  setMessage(state, value) {
    state.data = {
      ...value,
    }
  },
  resetMessage(state, value) {
    state.data = {
      type: '',
      msg: '',
    }
  },
}

export const actions = {
  addMessage(context, payload) {
    context.commit('setMessage', payload)
    setTimeout(
      () => {
        context.dispatch('removeMessage')
      },
      payload.duration ? payload.duration : 2000
    )
  },
  removeMessage(context) {
    context.commit('resetMessage')
  },
}
