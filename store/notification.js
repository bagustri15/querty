// holds your root state
export const state = () => ({
  listNotification: [],
})

// contains your mutations
export const mutations = {}

// contains your actions
export const actions = {}

// your root getters
export const getters = {}
