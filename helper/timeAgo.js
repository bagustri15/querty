const epochs = [
  ['tahun', 31536000],
  ['bulan', 2592000],
  ['hari', 86400],
  ['jam', 3600],
  ['menit', 60],
  ['detik', 1],
]

const getDuration = (timeAgoInSeconds) => {
  for (const [name, seconds] of epochs) {
    const interval = Math.floor(timeAgoInSeconds / seconds)
    if (interval >= 1) {
      return {
        interval,
        epoch: name,
      }
    }
  }
}

const timeAgo = (date) => {
  const timeAgoInSeconds = Math.floor((new Date() - new Date(date)) / 1000)
  const { interval, epoch } = getDuration(timeAgoInSeconds)
  return `${interval} ${epoch} yang lalu`
}

export default timeAgo
// https://stackoverflow.com/questions/3177836/how-to-format-time-since-xxx-e-g-4-minutes-ago-similar-to-stack-exchange-site
