export default function ({ store, redirect }) {
  const isLogin = store.state.user.token
  if (!isLogin) {
    return redirect('/login')
  }
}
