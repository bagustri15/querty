export default function ({ $axios, env, redirect, store }) {
  const token = store.state.user.token
  $axios.setBaseURL('https://jsonplaceholder.typicode.com')
  $axios.setToken(token, 'Bearer')
  $axios.onError((error) => {
    if (error.response.status === 404) {
      redirect('/sorry')
    }
  })
}

// https://stackoverflow.com/questions/68308060/how-to-use-nuxt-auth-inside-an-axios-plugin-how-to-add-token-to-all-axios-requ
